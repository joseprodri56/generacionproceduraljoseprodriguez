﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneracionProcedural2D : MonoBehaviour
{

    int seed= 5;
    public int width;

    public int height;

    //Gameobjects para instanciar
    
    public GameObject earth;
    //Grass es cesped, pls no te confundas xd.
    public GameObject grass;


    void Start()
    {
        //Los comentarios son ejemplos.
        //plane();
        //regular();
        perlinNoisemod();
        //mountain();
        //for(float i=0; i< width; i++)
        //{
            //print(Mathf.PerlinNoise(i/2,i/2));
        //}
    }

    private void perlinNoisemod()
    {
        for(int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                //Lo que he hecho es que haciendo el perlin noise, si el valor del float es mayor que 0.5, no instancie el cubo, y si no es mayor, que instancie
                //El valor siempre seran los mismos, pero tambien dependera de la seed (simplemente es un integer en el que multiplica el valor de las coordenadas que usan para hacer el perlin noise).
                //ALERTA, EL VALOR DE LA SEED NO PUEDE ACABAR EN 0 (0, 10, 20, 30). Sino el resultado sera un mapa plano. La seed no es perfecta, es solo un ejemplo
                float xCoord = (float) x / width * seed;
                float yCoord = (float) y / height * seed;
                if(Mathf.PerlinNoise(xCoord, yCoord) >= 0.5f)
                {
                    
                    Debug.Log(1);
                }
                else
                {
                    GameObject earthInst = Instantiate(earth);
                    earthInst.transform.position = new Vector2(x, y);
                    Debug.Log(0);
                }
                //Debug.Log(Mathf.PerlinNoise(xCoord, yCoord));
                

            }
        }
    }

    private void regular()
    {
        //Voy a nombrarles x y en vez de i j para no liarme.
        for (int x = 0; x < width; x++)
        {
            //Debug.Log(i);
            //Nota: si hago un -1 +1 siempre se ira bajando el height, ademas de que constantemente se va a cambiar el valor del height. Asi que es mejor hacer un -1 +2.
            //No importa cuanto restes y sumes. Siempre dara un buen resultado siempre que sumes mas que restar
            int minHeight = height - 1;
            int maxHeight = height + 2;
            height = Random.Range(minHeight, maxHeight);

            for (int y = 0; y < height; y++)
            {
                //Aqui instanciamos e indicamos la posicion
                GameObject earthInst = Instantiate(earth);
                //Date cuenta que el tamaño de la escala sera el tamaño del prefab, que por default es 1, 1.
                earthInst.transform.position = new Vector2(x, y);
            }
            GameObject grassInst = Instantiate(grass);
            //Hara lo mismo, pero con el grass. La diferencia en eso es que no hace falta un length.
            //Y como el for de la "y" cuenta del 0 al ultimo numero del height sin contar el numero suyo mismo (P.E, si es un 20, hara de 0 al 19), pues el numero del height que no lo cuenta el for sera la posicion de la "y" (P.E, si es un 20, la posicion x sera la que indique el for de la "x", y la "y" sera el 20 mismo, ya que lo ponemos cuando acaba de recorrer el for anterior).
            grassInst.transform.position = new Vector2(x, height);
            GameObject grassInst2 = Instantiate(grass);
            grassInst2.transform.position = new Vector2(x, height + 1);
        }
    }
    
    private void plane()
    {
        for (int x = 0; x < width; x++)
        {
            
            for (int y = 0; y < height; y++)
            {
                //Aqui instanciamos e indicamos la posicion
                GameObject earthInst = Instantiate(earth);
                //Date cuenta que el tamaño de la escala sera el tamaño del prefab, que por default es 1, 1.
                earthInst.transform.position = new Vector2(x, y);
            }
            GameObject grassInst = Instantiate(grass);
            //Hara lo mismo, pero con el grass. La diferencia en eso es que no hace falta un length.
            grassInst.transform.position = new Vector2(x, height);
            GameObject grassInst2 = Instantiate(grass);
            grassInst2.transform.position = new Vector2(x, height + 1);
        }
    }
    //Se ira subiendo
    private void mountain()
    {
        for (int x = 0; x < width; x++)
        {
            int minHeight = height - 1;
            int maxHeight = height + 3;
            height = Random.Range(minHeight, maxHeight);
            for (int y = 0; y < height; y++)
            {
                //Aqui instanciamos e indicamos la posicion
                GameObject earthInst = Instantiate(earth);
                //Date cuenta que el tamaño de la escala sera el tamaño del prefab, que por default es 1, 1.
                earthInst.transform.position = new Vector2(x, y);
            }
            GameObject grassInst = Instantiate(grass);
            //Hara lo mismo, pero con el grass. La diferencia en eso es que no hace falta un length.
            grassInst.transform.position = new Vector2(x, height);
            GameObject grassInst2 = Instantiate(grass);
            grassInst2.transform.position = new Vector2(x, height + 1);
        }
    }
}
