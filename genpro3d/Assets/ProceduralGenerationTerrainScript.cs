﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralGenerationTerrainScript : MonoBehaviour
{
    //Puedes modificarlos en el inspector, pero por si acaso le pongo un valor de default. Es recomendable este valor por default
    //ALERTA: NO LOS MODIFIQUES AL INICIAR, SE DESEQUILIBRARAN COSAS
    public int width= 128;
    public int height = 128;
    //depth es profundidad. DAT english. Mas grande es el valor, mas grande son las montañas
    public int depth= 20;
    //Escala del terreno, es como el espaciado que dejaras la montaña
    public int scale = 5;

    //Puedes usar el inspector para editar esto
    public float transformX;
    public float transformY;



    Terrain terrain;
    // Start is called before the first frame update
    void Start()
    {
        
        //Cogeremos el terraindata del terrain, tanto en esta linea como dentro de generate terrain
        
    }

    private void Update()
    {
        terrain = this.GetComponent<Terrain>();
        terrain.terrainData = GenerateTerrain(terrain.terrainData);
    }


    private TerrainData GenerateTerrain(TerrainData terrainData)
    {
        //Ajustara el tamaño del mapa
        terrainData.heightmapResolution = width + 1;


        //Tamaño del terreno.
        terrainData.size = new Vector3(width, depth, height);

        //Las 2 variables del principio es el punto de inicio donde editaras los heights
        terrainData.SetHeights(0, 0, GenerateHeights());
        return terrainData;

    }

    private float[,] GenerateHeights()
    {
        //Supondre que la , de dentro del float es como si dijeses que no tiene limite este array (no lo confundas con un arraylist).
        float[,] heights = new float[width, height];

        //Uso del perlin noise
        for(int x= 0; x < width; x++)
        {

            for(int y=0; y < height; y++)
            {
                //CalculateHeight devolvera un float en esa posicion exacta.
                heights[x, y] = CalculateHeight(x,y);
            }


        }


        return heights;


    }

    private float CalculateHeight(int x, int y)
    {
        //Aqui pasamos los valores como enteros para que no sean numeros exactos y den el mismo valor.
        float xCoord = (float) x / width * scale + transformX;
        float yCoord = (float) y / height * scale + transformY;
        //Debug.Log(Mathf.PerlinNoise(xCoord, yCoord));
        return Mathf.PerlinNoise(xCoord, yCoord);

    }
}
